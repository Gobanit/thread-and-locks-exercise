package com.randakm.sandbox.exercise;

public class SimpleExample3 {

    private static final int ITERATIONS = 100000;
    private static int x = 0;


    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(SimpleExample3::workerJob, "Worker1");
        t1.start();
        Thread t2 = new Thread(SimpleExample3::workerJob, "Worker2");
        t2.start();

        t1.join();
        t2.join();

        System.out.println("Threads finished!");
        System.out.println("X: " + x);
    }

    private static void workerJob() {
        for (int i = 0; i < ITERATIONS; i++) {
            System.out.println(x++);
        }
        System.out.println("Work job finished");
    }

}
