package com.randakm.sandbox.exercise;

public class SimpleExample5 {

    public static void main(String[] args) throws InterruptedException {
        var x = 10; // load(10), save(*x)
        var y = x + 5; // load(*x), add(5), save(*y)
        System.out.println(y); // load(*y), print()
        var z = 100; // load(100), save(*z)
        z++; // load(*z), add(1), save(*z)
        System.out.println(z + " " + (x + 1)); // load(*x), add(1), save(*tmp), load(z), add(" "), add(*tmp), print()
    }

}
