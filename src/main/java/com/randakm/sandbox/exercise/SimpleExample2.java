package com.randakm.sandbox.exercise;

public class SimpleExample2 {

    private static final int ITERATIONS = 1;
    private static int x = 0;
    private static int y = 10;


    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(SimpleExample2::workerJobA, "Worker1");
        t1.start();
        Thread t2 = new Thread(SimpleExample2::workerJobB, "Worker2");
        t2.start();

        t1.join();
        t2.join();
    }

    private static void workerJobA() {
        for (int i = 0; i < ITERATIONS; i++) {
            x = 0;
            y = 10;
            printState();
        }
    }

    private static void workerJobB() {
        for (int i = 0; i < ITERATIONS; i++) {
            x = 100;
            y = 110;
            printState();
        }
    }

    private static synchronized void printState() {
        String value = String.format("%s, X: %d, Y: %d", Thread.currentThread().getName(), x, y);
        System.out.println(value);
    }

}
