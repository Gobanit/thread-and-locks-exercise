package com.randakm.sandbox.exercise;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SimpleExample4 {

    private static final int ITERATIONS = 100000;
    private static final List<Integer> VALUES = Collections.synchronizedList(new ArrayList<>());
    private static int x = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(SimpleExample4::workerJob, "Worker1");
        t1.start();
        Thread t2 = new Thread(SimpleExample4::workerJob, "Worker2");
        t2.start();

        t1.join();
        t2.join();

        System.out.println("Threads finished!");
        System.out.println("X: " + x);

        Set<Integer> distinctValues = new HashSet<>(VALUES);

        System.out.println("Total size: " + VALUES.size());
        System.out.println("Distinct size: " + distinctValues.size());

        var duplicatedValues = findDuplicated();
        for (var value : duplicatedValues) {
            System.out.println("Duplicated value: " + value);
        }

    }

    private static Set<Integer> findDuplicated() {
        return VALUES.stream()
                .collect(Collectors.groupingBy(Function.identity()))
                .entrySet().stream()
                .filter(it -> it.getValue().size() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    private static void workerJob() {
        for (int i = 0; i < ITERATIONS; i++) {
            int v = x++;
            System.out.println(v);
            VALUES.add(v);
        }
        System.out.println("Work job finished");
    }

}
