package com.randakm.sandbox.exercise;

public class SimpleExample {
    public static void main(String[] args) {
        Thread t1 = new Thread(SimpleExample::workerJob, "Worker1");
        t1.start();
        Thread t2 = new Thread(SimpleExample::workerJob, "Worker2");
        t2.start();
    }

    private static void workerJob() {
        while (true) {
            useResource();
        }
    }

    private static void useResource() {
        String name = Thread.currentThread().getName();

        System.out.println("Thread " + name + " started resource usgage!");

        try {
            Thread.sleep(750);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Thread " + name + " finished resource usgage!");
    }


}